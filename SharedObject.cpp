//
// Created by mcrowe on 9/6/20.
//

#include "SharedObject.h"
std::vector<long> mcrowe::SharedObject::getTopTen() {
    std::vector<long> result;
    while (queue.size() > 10) {
        queue.pop();
    }
    while (!queue.empty()) {
        long next = queue.top();
        queue.pop();
        result.emplace_back(next);
    }
    return result;
}

void mcrowe::SharedObject::accumulate(long value) {
    lock.lock();
    queue.emplace(value);
    accumulator.fetch_add(value);
    primeCount.fetch_add(1);
    while (queue.size() > 16) {
        queue.pop();
    }
    lock.unlock();
}

long mcrowe::SharedObject::getAndIncrementCount() {
    long result;
    lock.lock();
    result = counter.fetch_add(2);
    lock.unlock();
    return result;
}

long mcrowe::SharedObject::getAccumulated() {
    return accumulator.operator unsigned long();
}

long mcrowe::SharedObject::getPrimeCount() {
    return primeCount.operator unsigned long();
}


# Prime Machine for 8-Core system.
The goal of this project is to check each number from 1 to 10<sup>8</sup> for primality while utilizing all
8 cores concurrently.  AKS was considered for the primality test, but is prone to coding errors, thus it is
better suited to use an approach that is easily followed.  Miller-Rabin has plenty of coded examples
posted online and was chosen as the approach to use for primality testing.  A deterministic version was
posted to RosettaCode.org which provided a more than acceptable solution for this task.  The code was written
 in C and was modified to change type of size_t to long and scrubbed for redundant logic and code formatting.
 
# Building and Executing
 
This project was developed using CLion.  It is assumed that the host system will have <code>gcc</code> and 
<code>make</code> available to build and compile the project. 
Building the project is as easy as:
```shell script
cd <project path>/cmake-build-debug
make
./COP6616_Assignment_1
```
This will result in the generation of the file <code>primes.txt</code> in the current directory.   

# Design Considerations
## Correctness.
### Overview - prime checking
This exercise makes no sense if the results are incorrect.  As such, it was tested in a small range for
correct values.  The first 1000 prime number sum is readily available with a quick google search and was 
found to be correct in when limiting the execution in a single thread test bed. The value is 76127 and the 
initial test in single threaded execution revealed this is the result.

By using a naive method for checking prime numbers, we can see that the output is indeed correct.
```text
primes.txt
1.04878 168 76127
937 941 947 953 967 971 977 983 991 997 
```

The naive method requires checking all numbers against the modulo of all known prime numbers.  This is massively 
inefficient to say the least.  A little googling lead to many more efficient methods.  AKS seemed to the best performing
probabilistic method, but the implementation behind it was quite complex.  Slightly less efficient was the Miller-Rabin
test, which is simply a fix for Fermat's little theorem.  It too is probabilistic, but there existed a deterministic 
variant posted on RosettaCode.org.
https://rosettacode.org/wiki/Miller%E2%80%93Rabin_primality_test#Deterministic_up_to_341.2C550.2C071.2C728.2C321 <br>
The code was reformatted for personal styling while limiting the upper end of values and redundant logic was collapsed 
to equivalent statements.  Also, <code>size_t</code> was replaced with <code>long</code> values.  Admittedly, 
<code>int</code> could probably have been used since we are only going to 10<sup>8</sup>.

Using naive method, single threaded:
+30 minutes, no output.

Using Miller-Rabin, single threaded:
```text
21555.3 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
Simply using a worthy primality test was the single biggest improvement.  This went from over 30 minutes to just over
21 seconds.

### Output values

This task is multithreaded, and as such, will be sharing a SharedObject. (see next section).  To guarantee that no WAR 
or RAW faults occur, mutual exlusion was implemented within the shared object to prevent competing threads from 
obliterating shared values.  The current number, accumulated total and count of prime numbers all fall into this 
category.  One requirement for this was that a list of the top ten primes are listed.  The easiest way to implement this
was through a priority queue.  Placing values into the queue bears no consequence on thread safety, so as long as all 
values make it into the underlying heap structure.  The only part that needs to be locked for thread safety is the 
maintaining the size of the queue.  Since it uses a heap under the covers, the next power of 2 over the number of values
required was chosen: <code>16</code>.  Simply stated, if a prime number is found, emplace it into the queue.  Then 
obtain a lock on the shared object altogether to record the value and maintain the queue.  This ensures that the count 
of primes is correct since we can guarantee that read/write to these atomic variables cannot be interfered with by other
threads.  Maintenance on the queue works since, no matter how many threads emplace a value, once a lock is given to a 
thread, the shared object will maintain the <code>16</code> items by removing items until we reach the correct size.  
Since the queue only removes the smallest item on <code>pop()</code>, we always maintain the top 16 largest values. Once
all the threads have completed their jobs, we pop all elements until the size is ten.  We collect the values into a 
vector by popping the remaining 10 elements in the queue.  This will reverse the order from descending to ascending,
thus providing the correct final output.

## Multithreading.
Following the guidance for this assignment, multithreading is another consideration that must be addressed.  In order to
satisfy the requirements of the output, there will need to be the following variables:<br>
<ul>
<li>count</li>
<li>sum</li>
<li>top ten primes</li>
</ul>
These variables result in a structure that should be shared among the threads doing work.  The should contain the proper
methods required to perform the tasks needed to record a prime number, keep track of which number to check and to 
extract the correct information when all threads complete.  Small optimizations were considered with respect to the 
initial values within this object.  For instance, we know that <code>2</code> is prime and is the first prime number.  
From this, we can initialize the sum with <code>2</code>.  Similarly, we can start at <code>3</code> and increment by 
steps of <code>2</code>.  Since we already account for the first prime, the count of primes can be initialized with the
value <code>1</code>.  The shared object will look like: 

~~~c++
class SharedObject {
    public:
    long getAndIncrementCount();
    void accumulate(long value);
    vector<long> getTopTen();
    long getAccumulated();
    long getPrimeCount();
~~~

This solution was tested with [1, 8] threads.

## Output.

The output will be placed in the same directory in which the executable is run.  The first value is the execution time.
It is derived from the <code>std::chrono</code> library and is the difference between system time right before the 
threads were created and the system time when the last thread joins.  The second value is the number of prime numbers 
found by all threads and the third is the sum of all the prime numbers found.  The last line lists the top ten prime 
numbers found in ascending order.

# Results
<ul>
<li>1 Thread<br>Average runtime: 21604.3ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
21520.7 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989  
```
</li>

<li>

```text
21594.2 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
21640.8 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
21606.3 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
21659.5 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

<ul>
<li>2 Threads<br>Average runtime: 19404.8ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
19502.7 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>

<li>

```text
19637 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
19298.2 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
19448.5 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
19137.4 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

<ul>
<li>3 Threads<br>Average runtime: 18076.1ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
18350.2 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>

<li>

```text
18129.5 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
17836.1 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
18038.1 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
18026.4 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

<ul>
<li>4 Threads<br>Average runtime: 16786.6ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
16679.5 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>

<li>

```text
16788.9 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16793.9 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16806 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16864.7 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

<ul>
<li>5 Threads<br>Average runtime: 16143.48ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
16207.8 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>

<li>

```text
16122.4 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16225.2 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16115.6 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16046.4 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

<ul>
<li>6 Threads<br>Average runtime: 16109.3ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
16188.8 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>

<li>

```text
16068.4 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16089.1 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16009.6 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16190.5 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

<ul>
<li>7 Threads<br>Average runtime: 16788.2ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
17050.1 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>

<li>

```text
17121.6 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16957.9 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16910.6 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
16900.6 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

<ul>
<li>8 Threads<br>Average runtime: 17700.7ms<br>Runs:</li>
</ul>
<ol>
<li>

```text
17720.9 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>

<li>

```text
17745.7 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
17655.4 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
17750.6 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
<li>

```text
17630.7 5761455 279209790387276
99999787 99999821 99999827 99999839 99999847 99999931 99999941 99999959 99999971 99999989 
```
</li>
</ol>

### Conclusion

Based on the average runtimes, I would recommend to my boss that we not use all 8 cores.  6 threads has the best overall
runtime, thus it follows my recommendation would be to use 6 cores to solve this problem.  This is most likely
due to overcontention on the shared object.  Thus, with most multicore programming, one should seek the balance between
parallelism and efficiency.  Too many threads costs too much for the OS to schedule and imparts extra overhead on the 
scheduler to keep them busy.



//
// Created by mcrowe on 8/31/20.
//

#ifndef COP6616_ASSIGNMENT_1_DETMILLERRABIN_H
#define COP6616_ASSIGNMENT_1_DETMILLERRABIN_H

#include <atomic>
#include <queue>
#include "../SharedObject.h"


/**
 * This class is inspired by code posted on rosettacode.org.  Typically, Miller-Rabin is considered a probabilistic
 * primality test.  However, a deterministic implementation was posted in C.  That code has been modified to fill the
 * interface class and formatted to use C++ styles and types.
 * source code referenced located at:
 * https://rosettacode.org/wiki/Miller%E2%80%93Rabin_primality_test#Deterministic_up_to_341.2C550.2C071.2C728.2C321
 */
namespace mcrowe {
    class DetMillerRabin {
    public:
        DetMillerRabin(mcrowe::SharedObject *sharedObject,
                       int LIMIT);
        void run();


    private:
        bool isPrime(long value);
        mcrowe::SharedObject *sharedObject;
        int LIMIT;
    };


}

#endif //COP6616_ASSIGNMENT_1_DETMILLERRABIN_H

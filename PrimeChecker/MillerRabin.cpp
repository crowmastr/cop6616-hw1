//
// Created by mcrowe on 9/1/20.
//

#include "MillerRabin.h"
#include <random>
#include <math.h>


mcrowe::MillerRabin::MillerRabin() {

}

mcrowe::MillerRabin::~MillerRabin() {

}

/**
 * Method for prime checking.  Implements the Miller-Rabin test as described on
 * https://www.youtube.com/watch?v=T6yDwcUvm7s.  content provided by dionyziz.
 */
bool mcrowe::MillerRabin::isPrime(long value) {
    int iterations = 25; // 25^(-iterations) chance of being incorrect.
    long phi, d;
    phi = d = value - 1;
    int r = 0;
    dist = std::uniform_int_distribution<int> (2, value - 2);


    while (d % 2 == 0) {
        d = d / 2;
        r = r + 1;
    }

    for (int i = 0; i < iterations; ++i) {
        long a = dist(rd);
        long exp = ((long)std::pow(a, d)) % phi;
        if (exp == 1 || exp == -1) {
            continue;
        }
        bool broken = false;
        for (int j = 1; j < r; ++j) {
            exp = ((long)std::pow(exp, 2)) % phi;
            if (exp == 1) {
                return false;
            }
            if (exp == -1) {
                broken = true;
                break;
            }
        }
        if (broken) {
            return false;
        }
    }
    return true;
}


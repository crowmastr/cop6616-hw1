//
// Created by mcrowe on 8/30/20.
//

#include "NaivePrimeChecker.h"


bool mcrowe::NaivePrimeChecker::isPrime(long value) {
    bool isPrime = false;
    if (value < 2 || value % 2 == 0) {
        isPrime = false;
    } else if (value == 2) {
        isPrime = true;
    } else {
        for (int x : set) {
            if (value % x == 0) {
                return false;
            }
        }
        set.insert(value);
        return true;
    }

    return isPrime;
}

mcrowe::NaivePrimeChecker::NaivePrimeChecker(mcrowe::SharedObject *sharedObject, int LIMIT) {
    this->sharedObject = sharedObject;
    this->LIMIT = LIMIT;
}

void mcrowe::NaivePrimeChecker::run() {
    long number = sharedObject->getAndIncrementCount();
    bool done = number >= LIMIT;
    while (!done) {
        if (isPrime(number)) {
            sharedObject->accumulate(number);
        }
        number = sharedObject->getAndIncrementCount();
        if (number >= LIMIT) {
            done = true;
        }
    }
}

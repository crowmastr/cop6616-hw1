//
// Created by mcrowe on 8/30/20.
//

#ifndef COP6616_ASSIGNMENT_1_NAIVEPRIMECHECKER_H
#define COP6616_ASSIGNMENT_1_NAIVEPRIMECHECKER_H
#include <set>
#include "../SharedObject.h"

namespace mcrowe {
    class NaivePrimeChecker {
    public:
        NaivePrimeChecker(mcrowe::SharedObject *sharedObject,
                          int LIMIT);
        void run();

    private:
        bool isPrime(long value);
        mcrowe::SharedObject *sharedObject;
        int LIMIT;
        std::set<long> set;
    };
}

#endif //COP6616_ASSIGNMENT_1_NAIVEPRIMECHECKER_H

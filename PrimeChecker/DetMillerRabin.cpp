//
// Created by mcrowe on 8/31/20.
//

#include "DetMillerRabin.h"

/**
 * This implementation is inspired by code posted on rosettacode.org.  Typically, Miller-Rabin is considered a
 * probabilistic primality test.  However, a deterministic implementation was posted in C.  Modifications include
 * removal of size_t and change to long, limiting range to useful limits, and reducing to logical equivalences where
 * appropriate.
 * source code referenced located at:
 * https://rosettacode.org/wiki/Miller%E2%80%93Rabin_primality_test#Deterministic_up_to_341.2C550.2C071.2C728.2C321
 */
long power(long a, long n, long mod) {
    long power = a;
    long result = 1;
    while (n > 0) {
        if (n & 1) {
            result = (result * power) % mod;
        }
        power = (power * power) % mod;
        n = n >> 1;
    }
    return result;
}

bool witness(long mod, long s, long d, long a) {
    long x = power(a, d, mod);
    long y;
    while (s) {
        y = (x * x) % mod;
        if (y == 1 && x != 1 && x != mod - 1) {
            return false;
        }
        x = y;
        --s;
    }
    return y == 1;
}

bool mcrowe::DetMillerRabin::isPrime(long value) {
    if (value == 2 || value == 3) {
        return true;
    }
    if (value <= 1 || !value || value % 3 == 0) {
        return false;
    }
    long d = value / 2;
    long s = 1;
    while (!(d & 1)) {
        d /= 2;
        ++s;
    }
    if (value < 1373653) {
        return witness(value, s, d, 2) && witness(value, s, d, 3);
    }
    if (value < 9080191) {
        return witness(value, s, d, 31) && witness(value, s, d, 73);
    }
    if (value < 4759123141) {
        return witness(value, s, d, 2) && witness(value, s, d, 7) && witness(value, s, d, 61);
    }
    return witness(value, s, d, 2) && witness(value, s, d, 3) && witness(value, s, d, 5) &&
            witness(value, s, d, 7) && witness(value, s, d, 11) && witness(value, s, d, 13) &&
            witness(value, s, d, 17);
}

mcrowe::DetMillerRabin::DetMillerRabin(mcrowe::SharedObject *sharedObject,
                                       int LIMIT) {
    this->sharedObject = sharedObject;
    this->LIMIT = LIMIT;
}

void mcrowe::DetMillerRabin::run() {
    long number = sharedObject->getAndIncrementCount();
    bool done = number >= LIMIT;
    while (!done) {
        if (isPrime(number)) {
            sharedObject->accumulate(number);
        }
        number = sharedObject->getAndIncrementCount();
        if (number >= LIMIT) {
            done = true;
        }
    }
}
/*
 * END REFERENCED CODE
 */


//
// Created by mcrowe on 9/1/20.
//

#ifndef COP6616_ASSIGNMENT_1_MILLERRABIN_H
#define COP6616_ASSIGNMENT_1_MILLERRABIN_H

#include <random>
#include <math.h>

/**
 * Class with single method for prime checking.  Implements the Miller-Rabin test as described on
 * https://www.youtube.com/watch?v=T6yDwcUvm7s.  content provided by dionyziz.
 */
namespace mcrowe {
    class MillerRabin {
    public:
        MillerRabin();
        ~MillerRabin();
        bool isPrime(long value);

    private:
        std::random_device rd;
        std::uniform_int_distribution<int> dist;
    };


}


#endif //COP6616_ASSIGNMENT_1_MILLERRABIN_H

//
// Created by mcrowe on 8/25/20.
//
#include <iostream>
#include <vector>
#include <thread>
#include <cmath>
#include <algorithm>
#include "PrimeChecker/DetMillerRabin.h"
#include "PrimeChecker/NaivePrimeChecker.h"
#include <chrono>
#include <fstream>

const int MAX_THREADS = 8;
const int LIMIT = pow(10, 8);
const std::string FILENAME = "primes.txt";

// utility function
void joinThread(std::thread& t) {
    t.join();
}

int main() {
    auto *sharedObject = new mcrowe::SharedObject;
    auto *checker = new mcrowe::DetMillerRabin(sharedObject, LIMIT);
//    auto *checker = new mcrowe::NaivePrimeChecker(sharedObject, LIMIT);
    std::vector<std::thread> threads;
    threads.reserve(MAX_THREADS);

    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < MAX_THREADS; ++i) {
        threads.emplace_back(std::thread(&mcrowe::DetMillerRabin::run, checker));
//        threads.emplace_back(std::thread(&mcrowe::NaivePrimeChecker::run, checker));
    }
    std::for_each(threads.begin(), threads.end(), joinThread);
    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> timeTaken = end - start;
    // total should be : 279209790387276
    std::ofstream output;
    output.open(FILENAME);
    output << timeTaken.count() << " " << sharedObject->getPrimeCount() << " " << sharedObject->getAccumulated() << std::endl;
    for (long value : sharedObject->getTopTen()) {
        output << value << " ";
    }
    output.close();
    return 0;
}


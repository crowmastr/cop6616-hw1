//
// Created by mcrowe on 9/6/20.
//

#ifndef COP6616_ASSIGNMENT_1_SHAREDOBJECT_H
#define COP6616_ASSIGNMENT_1_SHAREDOBJECT_H

#include <vector>
#include <functional>
#include <queue>
#include <atomic>
#include <mutex>

namespace mcrowe {
    class SharedObject {
    public:
        long getAndIncrementCount();
        void accumulate(long value);
        std::vector<long> getTopTen();
        long getAccumulated();
        long getPrimeCount();
    private:
        std::mutex lock;
        std::atomic_ulong counter{3};
        std::atomic_ulong accumulator{2};
        std::atomic_ulong primeCount{1};
        std::priority_queue<long, std::vector<long>, std::greater<long>> queue;
    };
}


#endif //COP6616_ASSIGNMENT_1_SHAREDOBJECT_H
